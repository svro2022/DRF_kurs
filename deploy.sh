# python3 -m venv env
# source env/bin/activate
# pip3 install -r requirements.txt
# python3 manage.py migrate
# deactivate

docker-compose up --build -d
docker-compose exec app python manage.py migrate
